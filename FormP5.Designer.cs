﻿namespace PruebaTFG_2
{
    partial class FormP5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormP5));
            this.panelTitulo = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panelControles = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnPrevio = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.panelPaginas = new System.Windows.Forms.Panel();
            this.panelPag1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panelPag2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelPag3 = new System.Windows.Forms.Panel();
            this.pBresolucion = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelPag4 = new System.Windows.Forms.Panel();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panelPag5 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panelPag6 = new System.Windows.Forms.Panel();
            this.lblOffsetVal = new System.Windows.Forms.Label();
            this.lblOffsetMax = new System.Windows.Forms.Label();
            this.lblOffsetMin = new System.Windows.Forms.Label();
            this.tBOffset = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.panelPag7 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panelPag8 = new System.Windows.Forms.Panel();
            this.lblFactorVal = new System.Windows.Forms.Label();
            this.lblFactorMax = new System.Windows.Forms.Label();
            this.lblFactorMin = new System.Windows.Forms.Label();
            this.tBFactor = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panelPag9 = new System.Windows.Forms.Panel();
            this.gBoxGanancia = new System.Windows.Forms.GroupBox();
            this.rBGain64 = new System.Windows.Forms.RadioButton();
            this.rBGain128 = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.panelTitulo.SuspendLayout();
            this.panelControles.SuspendLayout();
            this.panelPaginas.SuspendLayout();
            this.panelPag1.SuspendLayout();
            this.panelPag2.SuspendLayout();
            this.panelPag3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBresolucion)).BeginInit();
            this.panelPag4.SuspendLayout();
            this.panelPag5.SuspendLayout();
            this.panelPag6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tBOffset)).BeginInit();
            this.panelPag7.SuspendLayout();
            this.panelPag8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tBFactor)).BeginInit();
            this.panelPag9.SuspendLayout();
            this.gBoxGanancia.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTitulo
            // 
            this.panelTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelTitulo.Controls.Add(this.lblTitulo);
            this.panelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitulo.Location = new System.Drawing.Point(0, 0);
            this.panelTitulo.Name = "panelTitulo";
            this.panelTitulo.Size = new System.Drawing.Size(920, 100);
            this.panelTitulo.TabIndex = 3;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTitulo.Location = new System.Drawing.Point(353, 36);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(95, 29);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "HX711";
            // 
            // panelControles
            // 
            this.panelControles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelControles.Controls.Add(this.btnReset);
            this.panelControles.Controls.Add(this.btnPrevio);
            this.panelControles.Controls.Add(this.btnSiguiente);
            this.panelControles.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControles.Location = new System.Drawing.Point(0, 568);
            this.panelControles.Name = "panelControles";
            this.panelControles.Size = new System.Drawing.Size(920, 100);
            this.panelControles.TabIndex = 5;
            // 
            // btnReset
            // 
            this.btnReset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnReset.Location = new System.Drawing.Point(627, 26);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(137, 44);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "RESET";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnPrevio
            // 
            this.btnPrevio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnPrevio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrevio.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPrevio.Location = new System.Drawing.Point(47, 26);
            this.btnPrevio.Name = "btnPrevio";
            this.btnPrevio.Size = new System.Drawing.Size(137, 44);
            this.btnPrevio.TabIndex = 3;
            this.btnPrevio.Text = "Previo";
            this.btnPrevio.UseVisualStyleBackColor = true;
            this.btnPrevio.Click += new System.EventHandler(this.btnPrevio_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSiguiente.Location = new System.Drawing.Point(333, 26);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(137, 44);
            this.btnSiguiente.TabIndex = 2;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // panelPaginas
            // 
            this.panelPaginas.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelPaginas.Controls.Add(this.panelPag1);
            this.panelPaginas.Controls.Add(this.panelPag2);
            this.panelPaginas.Controls.Add(this.panelPag3);
            this.panelPaginas.Controls.Add(this.panelPag4);
            this.panelPaginas.Controls.Add(this.panelPag5);
            this.panelPaginas.Controls.Add(this.panelPag6);
            this.panelPaginas.Controls.Add(this.panelPag7);
            this.panelPaginas.Controls.Add(this.panelPag8);
            this.panelPaginas.Controls.Add(this.panelPag9);
            this.panelPaginas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPaginas.Location = new System.Drawing.Point(0, 100);
            this.panelPaginas.Name = "panelPaginas";
            this.panelPaginas.Size = new System.Drawing.Size(920, 468);
            this.panelPaginas.TabIndex = 7;
            // 
            // panelPag1
            // 
            this.panelPag1.BackColor = System.Drawing.Color.White;
            this.panelPag1.Controls.Add(this.label1);
            this.panelPag1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag1.Location = new System.Drawing.Point(0, 0);
            this.panelPag1.Name = "panelPag1";
            this.panelPag1.Size = new System.Drawing.Size(920, 468);
            this.panelPag1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Conecte el puente al HX711\r\n";
            // 
            // panelPag2
            // 
            this.panelPag2.BackColor = System.Drawing.Color.White;
            this.panelPag2.Controls.Add(this.label2);
            this.panelPag2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag2.Location = new System.Drawing.Point(0, 0);
            this.panelPag2.Name = "panelPag2";
            this.panelPag2.Size = new System.Drawing.Size(920, 468);
            this.panelPag2.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(433, 50);
            this.label2.TabIndex = 2;
            this.label2.Text = "Con el polimetro, mida la tensión del puente\r\n\r\n";
            // 
            // panelPag3
            // 
            this.panelPag3.BackColor = System.Drawing.Color.White;
            this.panelPag3.Controls.Add(this.pBresolucion);
            this.panelPag3.Controls.Add(this.label12);
            this.panelPag3.Controls.Add(this.label3);
            this.panelPag3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag3.Location = new System.Drawing.Point(0, 0);
            this.panelPag3.Name = "panelPag3";
            this.panelPag3.Size = new System.Drawing.Size(920, 468);
            this.panelPag3.TabIndex = 5;
            // 
            // pBresolucion
            // 
            this.pBresolucion.Image = ((System.Drawing.Image)(resources.GetObject("pBresolucion.Image")));
            this.pBresolucion.Location = new System.Drawing.Point(258, 196);
            this.pBresolucion.Name = "pBresolucion";
            this.pBresolucion.Size = new System.Drawing.Size(403, 209);
            this.pBresolucion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pBresolucion.TabIndex = 4;
            this.pBresolucion.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(300, 25);
            this.label12.TabIndex = 3;
            this.label12.Text = "Calcule la resolución del ADC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(806, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Sabiendo que la tensión del puente es la tensión de referencia del ADC del HX711";
            // 
            // panelPag4
            // 
            this.panelPag4.BackColor = System.Drawing.Color.White;
            this.panelPag4.Controls.Add(this.btnStop);
            this.panelPag4.Controls.Add(this.btnStart);
            this.panelPag4.Controls.Add(this.label4);
            this.panelPag4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag4.Location = new System.Drawing.Point(0, 0);
            this.panelPag4.Name = "panelPag4";
            this.panelPag4.Size = new System.Drawing.Size(920, 468);
            this.panelPag4.TabIndex = 7;
            // 
            // btnStop
            // 
            this.btnStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(17, 157);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(137, 44);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(17, 98);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(137, 44);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(398, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "Inicie la transmisión de datos del HX711";
            // 
            // panelPag5
            // 
            this.panelPag5.BackColor = System.Drawing.Color.White;
            this.panelPag5.Controls.Add(this.label7);
            this.panelPag5.Controls.Add(this.label6);
            this.panelPag5.Controls.Add(this.label5);
            this.panelPag5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag5.Location = new System.Drawing.Point(0, 0);
            this.panelPag5.Name = "panelPag5";
            this.panelPag5.Size = new System.Drawing.Size(920, 468);
            this.panelPag5.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(287, 25);
            this.label7.TabIndex = 4;
            this.label7.Text = "¿Está equilibrado el puente?\r\n";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(287, 25);
            this.label6.TabIndex = 3;
            this.label6.Text = "Observe la salida del puente";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(301, 25);
            this.label5.TabIndex = 2;
            this.label5.Text = "Quite todo de la viga de carga";
            // 
            // panelPag6
            // 
            this.panelPag6.BackColor = System.Drawing.Color.White;
            this.panelPag6.Controls.Add(this.lblOffsetVal);
            this.panelPag6.Controls.Add(this.lblOffsetMax);
            this.panelPag6.Controls.Add(this.lblOffsetMin);
            this.panelPag6.Controls.Add(this.tBOffset);
            this.panelPag6.Controls.Add(this.label10);
            this.panelPag6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag6.Location = new System.Drawing.Point(0, 0);
            this.panelPag6.Name = "panelPag6";
            this.panelPag6.Size = new System.Drawing.Size(920, 468);
            this.panelPag6.TabIndex = 9;
            // 
            // lblOffsetVal
            // 
            this.lblOffsetVal.AutoSize = true;
            this.lblOffsetVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOffsetVal.Location = new System.Drawing.Point(413, 196);
            this.lblOffsetVal.Name = "lblOffsetVal";
            this.lblOffsetVal.Size = new System.Drawing.Size(17, 17);
            this.lblOffsetVal.TabIndex = 19;
            this.lblOffsetVal.Text = "0";
            // 
            // lblOffsetMax
            // 
            this.lblOffsetMax.AutoSize = true;
            this.lblOffsetMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOffsetMax.Location = new System.Drawing.Point(780, 177);
            this.lblOffsetMax.Name = "lblOffsetMax";
            this.lblOffsetMax.Size = new System.Drawing.Size(61, 17);
            this.lblOffsetMax.TabIndex = 18;
            this.lblOffsetMax.Text = "label15";
            // 
            // lblOffsetMin
            // 
            this.lblOffsetMin.AutoSize = true;
            this.lblOffsetMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOffsetMin.Location = new System.Drawing.Point(12, 177);
            this.lblOffsetMin.Name = "lblOffsetMin";
            this.lblOffsetMin.Size = new System.Drawing.Size(61, 17);
            this.lblOffsetMin.TabIndex = 17;
            this.lblOffsetMin.Text = "label14";
            // 
            // tBOffset
            // 
            this.tBOffset.BackColor = System.Drawing.Color.White;
            this.tBOffset.Cursor = System.Windows.Forms.Cursors.Default;
            this.tBOffset.Location = new System.Drawing.Point(105, 157);
            this.tBOffset.Maximum = 30000;
            this.tBOffset.Minimum = -30000;
            this.tBOffset.Name = "tBOffset";
            this.tBOffset.Size = new System.Drawing.Size(632, 56);
            this.tBOffset.TabIndex = 16;
            this.tBOffset.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tBOffset.ValueChanged += new System.EventHandler(this.tBOffset_ValueChanged);
            this.tBOffset.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tBOffset_KeyUp);
            this.tBOffset.MouseCaptureChanged += new System.EventHandler(this.tBOffset_MouseCaptureChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(620, 25);
            this.label10.TabIndex = 2;
            this.label10.Text = "Mediante el deslizador, ajuste el offset para equilibrar el puente";
            // 
            // panelPag7
            // 
            this.panelPag7.BackColor = System.Drawing.Color.White;
            this.panelPag7.Controls.Add(this.label8);
            this.panelPag7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag7.Location = new System.Drawing.Point(0, 0);
            this.panelPag7.Name = "panelPag7";
            this.panelPag7.Size = new System.Drawing.Size(920, 468);
            this.panelPag7.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(633, 25);
            this.label8.TabIndex = 2;
            this.label8.Text = "Coloque los pesos calibrados y obtenga una recta de calibración";
            // 
            // panelPag8
            // 
            this.panelPag8.BackColor = System.Drawing.Color.White;
            this.panelPag8.Controls.Add(this.lblFactorVal);
            this.panelPag8.Controls.Add(this.lblFactorMax);
            this.panelPag8.Controls.Add(this.lblFactorMin);
            this.panelPag8.Controls.Add(this.tBFactor);
            this.panelPag8.Controls.Add(this.label11);
            this.panelPag8.Controls.Add(this.label9);
            this.panelPag8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag8.Location = new System.Drawing.Point(0, 0);
            this.panelPag8.Name = "panelPag8";
            this.panelPag8.Size = new System.Drawing.Size(920, 468);
            this.panelPag8.TabIndex = 11;
            // 
            // lblFactorVal
            // 
            this.lblFactorVal.AutoSize = true;
            this.lblFactorVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFactorVal.Location = new System.Drawing.Point(415, 196);
            this.lblFactorVal.Name = "lblFactorVal";
            this.lblFactorVal.Size = new System.Drawing.Size(17, 17);
            this.lblFactorVal.TabIndex = 20;
            this.lblFactorVal.Text = "1";
            // 
            // lblFactorMax
            // 
            this.lblFactorMax.AutoSize = true;
            this.lblFactorMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFactorMax.Location = new System.Drawing.Point(780, 177);
            this.lblFactorMax.Name = "lblFactorMax";
            this.lblFactorMax.Size = new System.Drawing.Size(61, 17);
            this.lblFactorMax.TabIndex = 19;
            this.lblFactorMax.Text = "label15";
            // 
            // lblFactorMin
            // 
            this.lblFactorMin.AutoSize = true;
            this.lblFactorMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFactorMin.Location = new System.Drawing.Point(14, 177);
            this.lblFactorMin.Name = "lblFactorMin";
            this.lblFactorMin.Size = new System.Drawing.Size(61, 17);
            this.lblFactorMin.TabIndex = 18;
            this.lblFactorMin.Text = "label14";
            // 
            // tBFactor
            // 
            this.tBFactor.BackColor = System.Drawing.Color.White;
            this.tBFactor.Cursor = System.Windows.Forms.Cursors.Default;
            this.tBFactor.Location = new System.Drawing.Point(118, 157);
            this.tBFactor.Maximum = 2000;
            this.tBFactor.Minimum = 1;
            this.tBFactor.Name = "tBFactor";
            this.tBFactor.Size = new System.Drawing.Size(610, 56);
            this.tBFactor.TabIndex = 17;
            this.tBFactor.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tBFactor.Value = 10;
            this.tBFactor.ValueChanged += new System.EventHandler(this.tBFactor_ValueChanged);
            this.tBFactor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tBFactor_KeyUp);
            this.tBFactor.MouseCaptureChanged += new System.EventHandler(this.tBFactor_MouseCaptureChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(646, 25);
            this.label11.TabIndex = 3;
            this.label11.Text = "Con los pesos calibrados y el deslizador ajuste el factor de escala";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(525, 25);
            this.label9.TabIndex = 2;
            this.label9.Text = "Se desea obtener una salida directamente en gramos";
            // 
            // panelPag9
            // 
            this.panelPag9.BackColor = System.Drawing.Color.White;
            this.panelPag9.Controls.Add(this.gBoxGanancia);
            this.panelPag9.Controls.Add(this.label13);
            this.panelPag9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag9.Location = new System.Drawing.Point(0, 0);
            this.panelPag9.Name = "panelPag9";
            this.panelPag9.Size = new System.Drawing.Size(920, 468);
            this.panelPag9.TabIndex = 12;
            // 
            // gBoxGanancia
            // 
            this.gBoxGanancia.Controls.Add(this.rBGain64);
            this.gBoxGanancia.Controls.Add(this.rBGain128);
            this.gBoxGanancia.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxGanancia.Location = new System.Drawing.Point(308, 132);
            this.gBoxGanancia.Name = "gBoxGanancia";
            this.gBoxGanancia.Size = new System.Drawing.Size(162, 132);
            this.gBoxGanancia.TabIndex = 5;
            this.gBoxGanancia.TabStop = false;
            this.gBoxGanancia.Text = "Ganancia";
            // 
            // rBGain64
            // 
            this.rBGain64.AutoSize = true;
            this.rBGain64.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBGain64.Location = new System.Drawing.Point(6, 76);
            this.rBGain64.Name = "rBGain64";
            this.rBGain64.Size = new System.Drawing.Size(77, 33);
            this.rBGain64.TabIndex = 1;
            this.rBGain64.Text = "x64";
            this.rBGain64.UseVisualStyleBackColor = true;
            // 
            // rBGain128
            // 
            this.rBGain128.AutoSize = true;
            this.rBGain128.Checked = true;
            this.rBGain128.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBGain128.Location = new System.Drawing.Point(6, 37);
            this.rBGain128.Name = "rBGain128";
            this.rBGain128.Size = new System.Drawing.Size(92, 33);
            this.rBGain128.TabIndex = 0;
            this.rBGain128.TabStop = true;
            this.rBGain128.Text = "x128";
            this.rBGain128.UseVisualStyleBackColor = true;
            this.rBGain128.CheckedChanged += new System.EventHandler(this.rBGain128_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(12, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(649, 25);
            this.label13.TabIndex = 2;
            this.label13.Text = "Cambie el valor de la ganancia del amplificador y repita el proceso";
            // 
            // FormP5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 668);
            this.Controls.Add(this.panelPaginas);
            this.Controls.Add(this.panelTitulo);
            this.Controls.Add(this.panelControles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormP5";
            this.Text = "FormP5";
            this.panelTitulo.ResumeLayout(false);
            this.panelTitulo.PerformLayout();
            this.panelControles.ResumeLayout(false);
            this.panelPaginas.ResumeLayout(false);
            this.panelPag1.ResumeLayout(false);
            this.panelPag1.PerformLayout();
            this.panelPag2.ResumeLayout(false);
            this.panelPag2.PerformLayout();
            this.panelPag3.ResumeLayout(false);
            this.panelPag3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBresolucion)).EndInit();
            this.panelPag4.ResumeLayout(false);
            this.panelPag4.PerformLayout();
            this.panelPag5.ResumeLayout(false);
            this.panelPag5.PerformLayout();
            this.panelPag6.ResumeLayout(false);
            this.panelPag6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tBOffset)).EndInit();
            this.panelPag7.ResumeLayout(false);
            this.panelPag7.PerformLayout();
            this.panelPag8.ResumeLayout(false);
            this.panelPag8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tBFactor)).EndInit();
            this.panelPag9.ResumeLayout(false);
            this.panelPag9.PerformLayout();
            this.gBoxGanancia.ResumeLayout(false);
            this.gBoxGanancia.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTitulo;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel panelControles;
        private System.Windows.Forms.Button btnPrevio;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Panel panelPaginas;
        private System.Windows.Forms.Panel panelPag1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelPag2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelPag3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelPag4;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelPag5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panelPag6;
        private System.Windows.Forms.TrackBar tBOffset;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panelPag7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panelPag8;
        private System.Windows.Forms.TrackBar tBFactor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelPag9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox gBoxGanancia;
        private System.Windows.Forms.RadioButton rBGain64;
        private System.Windows.Forms.RadioButton rBGain128;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lblOffsetVal;
        private System.Windows.Forms.Label lblOffsetMax;
        private System.Windows.Forms.Label lblOffsetMin;
        private System.Windows.Forms.Label lblFactorVal;
        private System.Windows.Forms.Label lblFactorMax;
        private System.Windows.Forms.Label lblFactorMin;
        private System.Windows.Forms.PictureBox pBresolucion;
    }
}