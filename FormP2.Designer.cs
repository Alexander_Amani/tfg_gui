﻿namespace PruebaTFG_2
{
    partial class FormP2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormP2));
            this.panelTitulo = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panelPag1 = new System.Windows.Forms.Panel();
            this.pBPuente = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelControles = new System.Windows.Forms.Panel();
            this.btnPrevio = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.panelPag2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelPag3 = new System.Windows.Forms.Panel();
            this.pBTerminales = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panelPaginas = new System.Windows.Forms.Panel();
            this.panelPag4 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panelTitulo.SuspendLayout();
            this.panelPag1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBPuente)).BeginInit();
            this.panelControles.SuspendLayout();
            this.panelPag2.SuspendLayout();
            this.panelPag3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBTerminales)).BeginInit();
            this.panelPaginas.SuspendLayout();
            this.panelPag4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTitulo
            // 
            this.panelTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelTitulo.Controls.Add(this.lblTitulo);
            this.panelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitulo.Location = new System.Drawing.Point(0, 0);
            this.panelTitulo.Name = "panelTitulo";
            this.panelTitulo.Size = new System.Drawing.Size(945, 100);
            this.panelTitulo.TabIndex = 0;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(299, 33);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(356, 29);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "RESISTENCIA DE REPOSO";
            // 
            // panelPag1
            // 
            this.panelPag1.BackColor = System.Drawing.Color.White;
            this.panelPag1.Controls.Add(this.pBPuente);
            this.panelPag1.Controls.Add(this.label1);
            this.panelPag1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag1.Location = new System.Drawing.Point(0, 0);
            this.panelPag1.Name = "panelPag1";
            this.panelPag1.Size = new System.Drawing.Size(945, 493);
            this.panelPag1.TabIndex = 1;
            // 
            // pBPuente
            // 
            this.pBPuente.Image = ((System.Drawing.Image)(resources.GetObject("pBPuente.Image")));
            this.pBPuente.Location = new System.Drawing.Point(236, 110);
            this.pBPuente.Name = "pBPuente";
            this.pBPuente.Size = new System.Drawing.Size(442, 317);
            this.pBPuente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pBPuente.TabIndex = 2;
            this.pBPuente.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(697, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "En este apartado se pretende medir la resistencia de reposo del puente\r\n";
            // 
            // panelControles
            // 
            this.panelControles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelControles.Controls.Add(this.btnPrevio);
            this.panelControles.Controls.Add(this.btnSiguiente);
            this.panelControles.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControles.Location = new System.Drawing.Point(0, 593);
            this.panelControles.Name = "panelControles";
            this.panelControles.Size = new System.Drawing.Size(945, 100);
            this.panelControles.TabIndex = 2;
            // 
            // btnPrevio
            // 
            this.btnPrevio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnPrevio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrevio.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevio.ForeColor = System.Drawing.Color.White;
            this.btnPrevio.Location = new System.Drawing.Point(144, 26);
            this.btnPrevio.Name = "btnPrevio";
            this.btnPrevio.Size = new System.Drawing.Size(137, 44);
            this.btnPrevio.TabIndex = 3;
            this.btnPrevio.Text = "Previo";
            this.btnPrevio.UseVisualStyleBackColor = true;
            this.btnPrevio.Click += new System.EventHandler(this.btnPrevio_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiguiente.ForeColor = System.Drawing.Color.White;
            this.btnSiguiente.Location = new System.Drawing.Point(624, 26);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(137, 44);
            this.btnSiguiente.TabIndex = 2;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // panelPag2
            // 
            this.panelPag2.BackColor = System.Drawing.Color.White;
            this.panelPag2.Controls.Add(this.label3);
            this.panelPag2.Controls.Add(this.label2);
            this.panelPag2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelPag2.Location = new System.Drawing.Point(0, 0);
            this.panelPag2.Name = "panelPag2";
            this.panelPag2.Size = new System.Drawing.Size(945, 493);
            this.panelPag2.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(599, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Prepare el multímetro. Ponga el valor de resistencia más alto.\r\n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(343, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Desconecte el puente del circuito. ";
            // 
            // panelPag3
            // 
            this.panelPag3.BackColor = System.Drawing.Color.White;
            this.panelPag3.Controls.Add(this.pBTerminales);
            this.panelPag3.Controls.Add(this.label5);
            this.panelPag3.Controls.Add(this.label4);
            this.panelPag3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag3.Location = new System.Drawing.Point(0, 0);
            this.panelPag3.Name = "panelPag3";
            this.panelPag3.Size = new System.Drawing.Size(945, 493);
            this.panelPag3.TabIndex = 3;
            // 
            // pBTerminales
            // 
            this.pBTerminales.Image = ((System.Drawing.Image)(resources.GetObject("pBTerminales.Image")));
            this.pBTerminales.Location = new System.Drawing.Point(46, 198);
            this.pBTerminales.Name = "pBTerminales";
            this.pBTerminales.Size = new System.Drawing.Size(403, 274);
            this.pBTerminales.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pBTerminales.TabIndex = 3;
            this.pBTerminales.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(41, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(477, 25);
            this.label5.TabIndex = 2;
            this.label5.Text = "¿Qué valores ha obtenido? ¿Son todos iguales?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(41, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(614, 25);
            this.label4.TabIndex = 1;
            this.label4.Text = "Mida la resistencia entre cada pareja de terminales del puente.\r\n";
            // 
            // panelPaginas
            // 
            this.panelPaginas.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panelPaginas.Controls.Add(this.panelPag1);
            this.panelPaginas.Controls.Add(this.panelPag2);
            this.panelPaginas.Controls.Add(this.panelPag3);
            this.panelPaginas.Controls.Add(this.panelPag4);
            this.panelPaginas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPaginas.Location = new System.Drawing.Point(0, 100);
            this.panelPaginas.Name = "panelPaginas";
            this.panelPaginas.Size = new System.Drawing.Size(945, 493);
            this.panelPaginas.TabIndex = 4;
            // 
            // panelPag4
            // 
            this.panelPag4.BackColor = System.Drawing.Color.White;
            this.panelPag4.Controls.Add(this.label6);
            this.panelPag4.Controls.Add(this.label7);
            this.panelPag4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag4.Location = new System.Drawing.Point(0, 0);
            this.panelPag4.Name = "panelPag4";
            this.panelPag4.Size = new System.Drawing.Size(945, 493);
            this.panelPag4.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(41, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(802, 25);
            this.label6.TabIndex = 2;
            this.label6.Text = "Con los datos obtenidos ¿Cuál es el valor de resistencia en reposo de las galgas?" +
    "\r\n";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(41, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(697, 25);
            this.label7.TabIndex = 1;
            this.label7.Text = "Calcule la resistencia serie equivalente entre cada pareja de terminales\r\n";
            // 
            // FormP2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 693);
            this.Controls.Add(this.panelPaginas);
            this.Controls.Add(this.panelControles);
            this.Controls.Add(this.panelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormP2";
            this.Text = "FormP2";
            this.panelTitulo.ResumeLayout(false);
            this.panelTitulo.PerformLayout();
            this.panelPag1.ResumeLayout(false);
            this.panelPag1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBPuente)).EndInit();
            this.panelControles.ResumeLayout(false);
            this.panelPag2.ResumeLayout(false);
            this.panelPag2.PerformLayout();
            this.panelPag3.ResumeLayout(false);
            this.panelPag3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBTerminales)).EndInit();
            this.panelPaginas.ResumeLayout(false);
            this.panelPag4.ResumeLayout(false);
            this.panelPag4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTitulo;
        private System.Windows.Forms.Panel panelPag1;
        private System.Windows.Forms.Panel panelControles;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Button btnPrevio;
        private System.Windows.Forms.Panel panelPag2;
        private System.Windows.Forms.Panel panelPag3;
        private System.Windows.Forms.Panel panelPaginas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelPag4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pBPuente;
        private System.Windows.Forms.PictureBox pBTerminales;
    }
}