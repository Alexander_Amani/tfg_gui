﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaTFG_2
{
    public partial class FormP1 : Form
    {
        //lista con todos los paneles(páginas) del form:
        Paginas paginas;
        //Form padre:
        private FormMain FormFather;
        public FormP1(FormMain father)
        {
            InitializeComponent();
            //inicializamos los paneles:
            ListaPanelesInit();
            //
            FormFather = father;
        }

        private void FormP1_Load(object sender, EventArgs e)
        {
        }

        private void ListaPanelesInit() {
            //aqui inicializamos la lista de paneles (paginas):
            paginas = new Paginas();
            //agregamos todas las páginas que haya en panelPaginas.controles:
            foreach (Panel p in this.panelPaginas.Controls) {
                paginas.agregar(p);
            }
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            paginas.siguiente();
        }

        private void btnPrevio_Click(object sender, EventArgs e)
        {
            paginas.anterior();
        }

    }
}
