﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaTFG_2
{
    public partial class FormP4 : Form
    {
        //lista con todos los paneles(páginas) del form:
        Paginas paginas;
        //Form padre:
        private FormMain FormFather;

        public FormP4(FormMain father)
        {
            InitializeComponent();
            ListaPanelesInit();
            //valores de las lbl:
            lblOffsetMin.Text = ((float)tBOffsetAD620.Minimum/1000).ToString();
            lblOffsetMax.Text = ((float)tBOffsetAD620.Maximum/1000).ToString();
            lblFactorMin.Text = ((float)tBFactor.Minimum/10).ToString();
            lblFactorMax.Text = ((float)tBFactor.Maximum/10).ToString();

            //
            FormFather = father;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            paginas.siguiente();
        }

        private void btnPrevio_Click(object sender, EventArgs e)
        {
            paginas.anterior();
        }

        private void ListaPanelesInit()
        {
            //aqui inicializamos la lista de paneles (paginas):
            paginas = new Paginas();
            //agregamos todas las páginas que haya en panelPaginas.controles:
            foreach (Panel p in this.panelPaginas.Controls)
            {
                paginas.agregar(p);
            }
        }

        private void btnStartAD620_Click(object sender, EventArgs e)
        {
            /*en este handler se manda al micro al estado de AD620
             */
            FormFather.estadoAD620();
        }

        private void btnStopAD620_Click(object sender, EventArgs e)
        {
            FormFather.estadoReposo();
        }

        private void tBOffsetAD620_MouseCaptureChanged(object sender, EventArgs e)
        {
            /*handler para evento de SOLTAR TECLA MOUSE
             * no cambiar por scroll porque sino se envian muchos mensajes!
             */
            //el slider solo acepta enteros. dividir por factor:
            float value = (float)tBOffsetAD620.Value / 1000;
            FormFather.ajusteOffset(value);
        }

        private void tBOffsetAD620_KeyUp(object sender, KeyEventArgs e)
        {
            /*handler para evento de SOLTAR TECLA FLECHAS
             * no cambiar por scroll porque sino se envian muchos mensajes!
             */
            //el slider solo acepta enteros. dividir por factor:
            float value = (float)tBOffsetAD620.Value / 1000;
            FormFather.ajusteOffset(value);
        }

        private void tBFactor_MouseCaptureChanged(object sender, EventArgs e)
        {
            /*handler para evento de SOLTAR TECLA MOUSE
             * no cambiar por scroll porque sino se envian muchos mensajes!
             */
            //el slider solo acepta enteros. dividir por factor:
            float value = (float)tBFactor.Value / 10;
            FormFather.ajusteFactor(value);
        }

        private void tBFactor_KeyUp(object sender, KeyEventArgs e)
        {
            /*handler para evento de SOLTAR TECLA FLECHAS
             * no cambiar por scroll porque sino se envian muchos mensajes!
             */
            //el slider solo acepta enteros. dividir por factor:
            float value = (float)tBFactor.Value / 10;
            FormFather.ajusteFactor(value);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            /*evento de pulsar el botón de reset
             * se resetean los parámetros del amplificador
             */
            FormFather.resetParam();
        }

        private void tBOffsetAD620_ValueChanged(object sender, EventArgs e)
        {
            //cuando el valor cambia modificamos el valor de la etiqueta:
            lblOffsetVal.Text = ((float)tBOffsetAD620.Value/1000).ToString();
        }

        private void tBFactor_ValueChanged(object sender, EventArgs e)
        {
            lblFactorVal.Text = ((float)tBFactor.Value / 10).ToString();
        }

    }
}
