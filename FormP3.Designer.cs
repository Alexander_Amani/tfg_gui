﻿namespace PruebaTFG_2
{
    partial class FormP3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTitulo = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panelControles = new System.Windows.Forms.Panel();
            this.btnPrevio = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.panelPag1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panelPag2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDesconectar = new System.Windows.Forms.Button();
            this.btnConectar = new System.Windows.Forms.Button();
            this.btnPuertos = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panelPaginas = new System.Windows.Forms.Panel();
            this.panelTitulo.SuspendLayout();
            this.panelControles.SuspendLayout();
            this.panelPag1.SuspendLayout();
            this.panelPag2.SuspendLayout();
            this.panelPaginas.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTitulo
            // 
            this.panelTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelTitulo.Controls.Add(this.lblTitulo);
            this.panelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitulo.Location = new System.Drawing.Point(0, 0);
            this.panelTitulo.Name = "panelTitulo";
            this.panelTitulo.Size = new System.Drawing.Size(731, 100);
            this.panelTitulo.TabIndex = 1;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTitulo.Location = new System.Drawing.Point(301, 37);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(153, 29);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "CONEXIÓN";
            // 
            // panelControles
            // 
            this.panelControles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelControles.Controls.Add(this.btnPrevio);
            this.panelControles.Controls.Add(this.btnSiguiente);
            this.panelControles.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControles.Location = new System.Drawing.Point(0, 466);
            this.panelControles.Name = "panelControles";
            this.panelControles.Size = new System.Drawing.Size(731, 100);
            this.panelControles.TabIndex = 3;
            // 
            // btnPrevio
            // 
            this.btnPrevio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnPrevio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrevio.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPrevio.Location = new System.Drawing.Point(144, 26);
            this.btnPrevio.Name = "btnPrevio";
            this.btnPrevio.Size = new System.Drawing.Size(137, 44);
            this.btnPrevio.TabIndex = 3;
            this.btnPrevio.Text = "Previo";
            this.btnPrevio.UseVisualStyleBackColor = true;
            this.btnPrevio.Click += new System.EventHandler(this.btnPrevio_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSiguiente.Location = new System.Drawing.Point(419, 26);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(137, 44);
            this.btnSiguiente.TabIndex = 2;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // panelPag1
            // 
            this.panelPag1.BackColor = System.Drawing.Color.White;
            this.panelPag1.Controls.Add(this.label1);
            this.panelPag1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag1.Location = new System.Drawing.Point(0, 0);
            this.panelPag1.Name = "panelPag1";
            this.panelPag1.Size = new System.Drawing.Size(731, 366);
            this.panelPag1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(581, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Conecte el microcontrolador al ordenador con el cable USB\r\n";
            // 
            // panelPag2
            // 
            this.panelPag2.BackColor = System.Drawing.Color.White;
            this.panelPag2.Controls.Add(this.label3);
            this.panelPag2.Controls.Add(this.btnDesconectar);
            this.panelPag2.Controls.Add(this.btnConectar);
            this.panelPag2.Controls.Add(this.btnPuertos);
            this.panelPag2.Controls.Add(this.label2);
            this.panelPag2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag2.Location = new System.Drawing.Point(0, 0);
            this.panelPag2.Name = "panelPag2";
            this.panelPag2.Size = new System.Drawing.Size(731, 366);
            this.panelPag2.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(437, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "En caso de error, presione DESCONECTAR\r\n";
            // 
            // btnDesconectar
            // 
            this.btnDesconectar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnDesconectar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDesconectar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesconectar.Location = new System.Drawing.Point(520, 93);
            this.btnDesconectar.Name = "btnDesconectar";
            this.btnDesconectar.Size = new System.Drawing.Size(165, 44);
            this.btnDesconectar.TabIndex = 5;
            this.btnDesconectar.Text = "DESCONECTAR";
            this.btnDesconectar.UseVisualStyleBackColor = true;
            this.btnDesconectar.Click += new System.EventHandler(this.btnDesconectar_Click);
            // 
            // btnConectar
            // 
            this.btnConectar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnConectar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConectar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConectar.Location = new System.Drawing.Point(262, 93);
            this.btnConectar.Name = "btnConectar";
            this.btnConectar.Size = new System.Drawing.Size(165, 44);
            this.btnConectar.TabIndex = 4;
            this.btnConectar.Text = "CONECTAR";
            this.btnConectar.UseVisualStyleBackColor = true;
            this.btnConectar.Click += new System.EventHandler(this.btnConectar_Click);
            // 
            // btnPuertos
            // 
            this.btnPuertos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnPuertos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPuertos.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPuertos.Location = new System.Drawing.Point(17, 93);
            this.btnPuertos.Name = "btnPuertos";
            this.btnPuertos.Size = new System.Drawing.Size(165, 44);
            this.btnPuertos.TabIndex = 3;
            this.btnPuertos.Text = "PUERTOS";
            this.btnPuertos.UseVisualStyleBackColor = true;
            this.btnPuertos.Click += new System.EventHandler(this.btnPuertos_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(645, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Seleccione el puerto serie con PUERTOS y presione CONECTAR\r\n";
            // 
            // panelPaginas
            // 
            this.panelPaginas.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelPaginas.Controls.Add(this.panelPag1);
            this.panelPaginas.Controls.Add(this.panelPag2);
            this.panelPaginas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPaginas.Location = new System.Drawing.Point(0, 100);
            this.panelPaginas.Name = "panelPaginas";
            this.panelPaginas.Size = new System.Drawing.Size(731, 366);
            this.panelPaginas.TabIndex = 5;
            // 
            // FormP3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 566);
            this.Controls.Add(this.panelPaginas);
            this.Controls.Add(this.panelControles);
            this.Controls.Add(this.panelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormP3";
            this.Text = "FormP3";
            this.panelTitulo.ResumeLayout(false);
            this.panelTitulo.PerformLayout();
            this.panelControles.ResumeLayout(false);
            this.panelPag1.ResumeLayout(false);
            this.panelPag1.PerformLayout();
            this.panelPag2.ResumeLayout(false);
            this.panelPag2.PerformLayout();
            this.panelPaginas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTitulo;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel panelControles;
        private System.Windows.Forms.Button btnPrevio;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Panel panelPag1;
        private System.Windows.Forms.Panel panelPag2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelPaginas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDesconectar;
        private System.Windows.Forms.Button btnConectar;
        private System.Windows.Forms.Button btnPuertos;
    }
}