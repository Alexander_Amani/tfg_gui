﻿namespace PruebaTFG_2
{
    partial class FormMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelInfo = new System.Windows.Forms.Panel();
            this.btnP5 = new System.Windows.Forms.Button();
            this.btnP4 = new System.Windows.Forms.Button();
            this.btnP3 = new System.Windows.Forms.Button();
            this.btnP2 = new System.Windows.Forms.Button();
            this.btnP1 = new System.Windows.Forms.Button();
            this.panelTitulo = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panelContenido = new System.Windows.Forms.Panel();
            this.gBoxAD620 = new System.Windows.Forms.GroupBox();
            this.lblAD620Factor = new System.Windows.Forms.Label();
            this.lblAD620Offset = new System.Windows.Forms.Label();
            this.lblAD620Peso = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnStopAD620 = new System.Windows.Forms.Button();
            this.btnStartAD620 = new System.Windows.Forms.Button();
            this.gBoxHX711 = new System.Windows.Forms.GroupBox();
            this.lblHXFactor = new System.Windows.Forms.Label();
            this.lblHXOffset = new System.Windows.Forms.Label();
            this.lblHXPeso = new System.Windows.Forms.Label();
            this.lblHXBits = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblOffset = new System.Windows.Forms.Label();
            this.lblPeso = new System.Windows.Forms.Label();
            this.lblBits = new System.Windows.Forms.Label();
            this.btnStopHX = new System.Windows.Forms.Button();
            this.btnStartHX = new System.Windows.Forms.Button();
            this.gBoxComunicación = new System.Windows.Forms.GroupBox();
            this.comBoxPuertos = new System.Windows.Forms.ComboBox();
            this.btnDesconectar = new System.Windows.Forms.Button();
            this.btnConectar = new System.Windows.Forms.Button();
            this.btnPuertos = new System.Windows.Forms.Button();
            this.mySerial = new System.IO.Ports.SerialPort(this.components);
            this.panelInfo.SuspendLayout();
            this.panelTitulo.SuspendLayout();
            this.panelContenido.SuspendLayout();
            this.gBoxAD620.SuspendLayout();
            this.gBoxHX711.SuspendLayout();
            this.gBoxComunicación.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelInfo
            // 
            this.panelInfo.AutoScroll = true;
            this.panelInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelInfo.Controls.Add(this.btnP5);
            this.panelInfo.Controls.Add(this.btnP4);
            this.panelInfo.Controls.Add(this.btnP3);
            this.panelInfo.Controls.Add(this.btnP2);
            this.panelInfo.Controls.Add(this.btnP1);
            this.panelInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelInfo.Location = new System.Drawing.Point(0, 0);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(177, 417);
            this.panelInfo.TabIndex = 0;
            // 
            // btnP5
            // 
            this.btnP5.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnP5.FlatAppearance.BorderSize = 0;
            this.btnP5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnP5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnP5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnP5.Location = new System.Drawing.Point(0, 320);
            this.btnP5.Name = "btnP5";
            this.btnP5.Size = new System.Drawing.Size(175, 80);
            this.btnP5.TabIndex = 4;
            this.btnP5.Text = "HX711";
            this.btnP5.UseVisualStyleBackColor = true;
            this.btnP5.Click += new System.EventHandler(this.btnP5_Click);
            // 
            // btnP4
            // 
            this.btnP4.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnP4.FlatAppearance.BorderSize = 0;
            this.btnP4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnP4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnP4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnP4.Location = new System.Drawing.Point(0, 240);
            this.btnP4.Name = "btnP4";
            this.btnP4.Size = new System.Drawing.Size(175, 80);
            this.btnP4.TabIndex = 3;
            this.btnP4.Text = "AD620 PUENTE";
            this.btnP4.UseVisualStyleBackColor = true;
            this.btnP4.Click += new System.EventHandler(this.btnP4_Click);
            // 
            // btnP3
            // 
            this.btnP3.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnP3.FlatAppearance.BorderSize = 0;
            this.btnP3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnP3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnP3.Location = new System.Drawing.Point(0, 160);
            this.btnP3.Name = "btnP3";
            this.btnP3.Size = new System.Drawing.Size(175, 80);
            this.btnP3.TabIndex = 2;
            this.btnP3.Text = "CONEXIÓN";
            this.btnP3.UseVisualStyleBackColor = true;
            this.btnP3.Click += new System.EventHandler(this.btnP3_Click);
            // 
            // btnP2
            // 
            this.btnP2.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnP2.FlatAppearance.BorderSize = 0;
            this.btnP2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnP2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnP2.Location = new System.Drawing.Point(0, 80);
            this.btnP2.Name = "btnP2";
            this.btnP2.Size = new System.Drawing.Size(175, 80);
            this.btnP2.TabIndex = 1;
            this.btnP2.Text = "RESISTENCIA";
            this.btnP2.UseVisualStyleBackColor = true;
            this.btnP2.Click += new System.EventHandler(this.btnP2_Click);
            // 
            // btnP1
            // 
            this.btnP1.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnP1.FlatAppearance.BorderSize = 0;
            this.btnP1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnP1.Location = new System.Drawing.Point(0, 0);
            this.btnP1.Name = "btnP1";
            this.btnP1.Size = new System.Drawing.Size(175, 80);
            this.btnP1.TabIndex = 0;
            this.btnP1.Text = "AD620";
            this.btnP1.UseVisualStyleBackColor = true;
            this.btnP1.Click += new System.EventHandler(this.btnP1_Click);
            // 
            // panelTitulo
            // 
            this.panelTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelTitulo.Controls.Add(this.lblTitulo);
            this.panelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitulo.Location = new System.Drawing.Point(177, 0);
            this.panelTitulo.Name = "panelTitulo";
            this.panelTitulo.Size = new System.Drawing.Size(716, 80);
            this.panelTitulo.TabIndex = 2;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTitulo.Location = new System.Drawing.Point(180, 22);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(282, 29);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "PANEL DE CONTROL";
            // 
            // panelContenido
            // 
            this.panelContenido.BackColor = System.Drawing.Color.White;
            this.panelContenido.Controls.Add(this.gBoxAD620);
            this.panelContenido.Controls.Add(this.gBoxHX711);
            this.panelContenido.Controls.Add(this.gBoxComunicación);
            this.panelContenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenido.Location = new System.Drawing.Point(177, 80);
            this.panelContenido.Name = "panelContenido";
            this.panelContenido.Size = new System.Drawing.Size(716, 337);
            this.panelContenido.TabIndex = 3;
            // 
            // gBoxAD620
            // 
            this.gBoxAD620.BackColor = System.Drawing.SystemColors.Control;
            this.gBoxAD620.Controls.Add(this.lblAD620Factor);
            this.gBoxAD620.Controls.Add(this.lblAD620Offset);
            this.gBoxAD620.Controls.Add(this.lblAD620Peso);
            this.gBoxAD620.Controls.Add(this.label8);
            this.gBoxAD620.Controls.Add(this.label7);
            this.gBoxAD620.Controls.Add(this.label2);
            this.gBoxAD620.Controls.Add(this.btnStopAD620);
            this.gBoxAD620.Controls.Add(this.btnStartAD620);
            this.gBoxAD620.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gBoxAD620.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxAD620.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.gBoxAD620.Location = new System.Drawing.Point(6, 172);
            this.gBoxAD620.Name = "gBoxAD620";
            this.gBoxAD620.Size = new System.Drawing.Size(387, 160);
            this.gBoxAD620.TabIndex = 2;
            this.gBoxAD620.TabStop = false;
            this.gBoxAD620.Text = "AD620";
            // 
            // lblAD620Factor
            // 
            this.lblAD620Factor.AutoSize = true;
            this.lblAD620Factor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAD620Factor.Location = new System.Drawing.Point(144, 123);
            this.lblAD620Factor.Name = "lblAD620Factor";
            this.lblAD620Factor.Size = new System.Drawing.Size(17, 17);
            this.lblAD620Factor.TabIndex = 24;
            this.lblAD620Factor.Text = "0";
            // 
            // lblAD620Offset
            // 
            this.lblAD620Offset.AutoSize = true;
            this.lblAD620Offset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAD620Offset.Location = new System.Drawing.Point(144, 73);
            this.lblAD620Offset.Name = "lblAD620Offset";
            this.lblAD620Offset.Size = new System.Drawing.Size(17, 17);
            this.lblAD620Offset.TabIndex = 23;
            this.lblAD620Offset.Text = "0";
            // 
            // lblAD620Peso
            // 
            this.lblAD620Peso.AutoSize = true;
            this.lblAD620Peso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAD620Peso.Location = new System.Drawing.Point(144, 23);
            this.lblAD620Peso.Name = "lblAD620Peso";
            this.lblAD620Peso.Size = new System.Drawing.Size(17, 17);
            this.lblAD620Peso.TabIndex = 22;
            this.lblAD620Peso.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(252, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Factor (g/v)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(252, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Offset (bits)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(252, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Peso (g)";
            // 
            // btnStopAD620
            // 
            this.btnStopAD620.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnStopAD620.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStopAD620.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopAD620.Location = new System.Drawing.Point(6, 91);
            this.btnStopAD620.Name = "btnStopAD620";
            this.btnStopAD620.Size = new System.Drawing.Size(103, 58);
            this.btnStopAD620.TabIndex = 2;
            this.btnStopAD620.Text = "Stop";
            this.btnStopAD620.UseVisualStyleBackColor = true;
            this.btnStopAD620.Click += new System.EventHandler(this.btnStopAD620_Click);
            // 
            // btnStartAD620
            // 
            this.btnStartAD620.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnStartAD620.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartAD620.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartAD620.Location = new System.Drawing.Point(6, 23);
            this.btnStartAD620.Name = "btnStartAD620";
            this.btnStartAD620.Size = new System.Drawing.Size(103, 58);
            this.btnStartAD620.TabIndex = 1;
            this.btnStartAD620.Text = "Start";
            this.btnStartAD620.UseVisualStyleBackColor = true;
            this.btnStartAD620.Click += new System.EventHandler(this.btnStartAD620_Click);
            // 
            // gBoxHX711
            // 
            this.gBoxHX711.BackColor = System.Drawing.SystemColors.Control;
            this.gBoxHX711.Controls.Add(this.lblHXFactor);
            this.gBoxHX711.Controls.Add(this.lblHXOffset);
            this.gBoxHX711.Controls.Add(this.lblHXPeso);
            this.gBoxHX711.Controls.Add(this.lblHXBits);
            this.gBoxHX711.Controls.Add(this.label1);
            this.gBoxHX711.Controls.Add(this.lblOffset);
            this.gBoxHX711.Controls.Add(this.lblPeso);
            this.gBoxHX711.Controls.Add(this.lblBits);
            this.gBoxHX711.Controls.Add(this.btnStopHX);
            this.gBoxHX711.Controls.Add(this.btnStartHX);
            this.gBoxHX711.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gBoxHX711.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxHX711.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.gBoxHX711.Location = new System.Drawing.Point(399, 6);
            this.gBoxHX711.Name = "gBoxHX711";
            this.gBoxHX711.Size = new System.Drawing.Size(314, 326);
            this.gBoxHX711.TabIndex = 1;
            this.gBoxHX711.TabStop = false;
            this.gBoxHX711.Text = "HX711";
            // 
            // lblHXFactor
            // 
            this.lblHXFactor.AutoSize = true;
            this.lblHXFactor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHXFactor.Location = new System.Drawing.Point(41, 296);
            this.lblHXFactor.Name = "lblHXFactor";
            this.lblHXFactor.Size = new System.Drawing.Size(17, 17);
            this.lblHXFactor.TabIndex = 21;
            this.lblHXFactor.Text = "0";
            // 
            // lblHXOffset
            // 
            this.lblHXOffset.AutoSize = true;
            this.lblHXOffset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHXOffset.Location = new System.Drawing.Point(41, 245);
            this.lblHXOffset.Name = "lblHXOffset";
            this.lblHXOffset.Size = new System.Drawing.Size(17, 17);
            this.lblHXOffset.TabIndex = 20;
            this.lblHXOffset.Text = "0";
            // 
            // lblHXPeso
            // 
            this.lblHXPeso.AutoSize = true;
            this.lblHXPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHXPeso.Location = new System.Drawing.Point(41, 194);
            this.lblHXPeso.Name = "lblHXPeso";
            this.lblHXPeso.Size = new System.Drawing.Size(17, 17);
            this.lblHXPeso.TabIndex = 19;
            this.lblHXPeso.Text = "0";
            // 
            // lblHXBits
            // 
            this.lblHXBits.AutoSize = true;
            this.lblHXBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHXBits.Location = new System.Drawing.Point(41, 143);
            this.lblHXBits.Name = "lblHXBits";
            this.lblHXBits.Size = new System.Drawing.Size(17, 17);
            this.lblHXBits.TabIndex = 18;
            this.lblHXBits.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(183, 296);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Factor (bits/g)";
            // 
            // lblOffset
            // 
            this.lblOffset.AutoSize = true;
            this.lblOffset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOffset.Location = new System.Drawing.Point(183, 245);
            this.lblOffset.Name = "lblOffset";
            this.lblOffset.Size = new System.Drawing.Size(95, 17);
            this.lblOffset.TabIndex = 13;
            this.lblOffset.Text = "Offset (bits)";
            // 
            // lblPeso
            // 
            this.lblPeso.AutoSize = true;
            this.lblPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeso.Location = new System.Drawing.Point(183, 194);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(70, 17);
            this.lblPeso.TabIndex = 12;
            this.lblPeso.Text = "Peso (g)";
            // 
            // lblBits
            // 
            this.lblBits.AutoSize = true;
            this.lblBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBits.Location = new System.Drawing.Point(183, 143);
            this.lblBits.Name = "lblBits";
            this.lblBits.Size = new System.Drawing.Size(35, 17);
            this.lblBits.TabIndex = 11;
            this.lblBits.Text = "Bits";
            // 
            // btnStopHX
            // 
            this.btnStopHX.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnStopHX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStopHX.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopHX.Location = new System.Drawing.Point(183, 50);
            this.btnStopHX.Name = "btnStopHX";
            this.btnStopHX.Size = new System.Drawing.Size(103, 58);
            this.btnStopHX.TabIndex = 1;
            this.btnStopHX.Text = "Stop";
            this.btnStopHX.UseVisualStyleBackColor = true;
            this.btnStopHX.Click += new System.EventHandler(this.btnStopHX_Click);
            // 
            // btnStartHX
            // 
            this.btnStartHX.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnStartHX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartHX.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartHX.Location = new System.Drawing.Point(41, 50);
            this.btnStartHX.Name = "btnStartHX";
            this.btnStartHX.Size = new System.Drawing.Size(103, 58);
            this.btnStartHX.TabIndex = 0;
            this.btnStartHX.Text = "Start";
            this.btnStartHX.UseVisualStyleBackColor = true;
            this.btnStartHX.Click += new System.EventHandler(this.btnStartHX_Click);
            // 
            // gBoxComunicación
            // 
            this.gBoxComunicación.BackColor = System.Drawing.SystemColors.Control;
            this.gBoxComunicación.Controls.Add(this.comBoxPuertos);
            this.gBoxComunicación.Controls.Add(this.btnDesconectar);
            this.gBoxComunicación.Controls.Add(this.btnConectar);
            this.gBoxComunicación.Controls.Add(this.btnPuertos);
            this.gBoxComunicación.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gBoxComunicación.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBoxComunicación.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.gBoxComunicación.Location = new System.Drawing.Point(6, 6);
            this.gBoxComunicación.Name = "gBoxComunicación";
            this.gBoxComunicación.Size = new System.Drawing.Size(387, 160);
            this.gBoxComunicación.TabIndex = 0;
            this.gBoxComunicación.TabStop = false;
            this.gBoxComunicación.Text = "Comunicación";
            // 
            // comBoxPuertos
            // 
            this.comBoxPuertos.FormattingEnabled = true;
            this.comBoxPuertos.Location = new System.Drawing.Point(120, 50);
            this.comBoxPuertos.Name = "comBoxPuertos";
            this.comBoxPuertos.Size = new System.Drawing.Size(121, 24);
            this.comBoxPuertos.TabIndex = 3;
            // 
            // btnDesconectar
            // 
            this.btnDesconectar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnDesconectar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDesconectar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesconectar.Location = new System.Drawing.Point(255, 94);
            this.btnDesconectar.Name = "btnDesconectar";
            this.btnDesconectar.Size = new System.Drawing.Size(125, 58);
            this.btnDesconectar.TabIndex = 2;
            this.btnDesconectar.Text = "Desconectar";
            this.btnDesconectar.UseVisualStyleBackColor = true;
            this.btnDesconectar.Click += new System.EventHandler(this.btnDesconectar_Click);
            // 
            // btnConectar
            // 
            this.btnConectar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnConectar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConectar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConectar.Location = new System.Drawing.Point(130, 94);
            this.btnConectar.Name = "btnConectar";
            this.btnConectar.Size = new System.Drawing.Size(125, 58);
            this.btnConectar.TabIndex = 1;
            this.btnConectar.Text = "Conectar";
            this.btnConectar.UseVisualStyleBackColor = true;
            this.btnConectar.Click += new System.EventHandler(this.btnConectar_Click);
            // 
            // btnPuertos
            // 
            this.btnPuertos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnPuertos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPuertos.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPuertos.Location = new System.Drawing.Point(5, 94);
            this.btnPuertos.Name = "btnPuertos";
            this.btnPuertos.Size = new System.Drawing.Size(125, 58);
            this.btnPuertos.TabIndex = 0;
            this.btnPuertos.Text = "Puertos";
            this.btnPuertos.UseVisualStyleBackColor = true;
            this.btnPuertos.Click += new System.EventHandler(this.btnPuertos_Click);
            // 
            // mySerial
            // 
            this.mySerial.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.mySerial_DataReceived);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 417);
            this.Controls.Add(this.panelContenido);
            this.Controls.Add(this.panelTitulo);
            this.Controls.Add(this.panelInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormMain";
            this.Text = "Form1";
            this.panelInfo.ResumeLayout(false);
            this.panelTitulo.ResumeLayout(false);
            this.panelTitulo.PerformLayout();
            this.panelContenido.ResumeLayout(false);
            this.gBoxAD620.ResumeLayout(false);
            this.gBoxAD620.PerformLayout();
            this.gBoxHX711.ResumeLayout(false);
            this.gBoxHX711.PerformLayout();
            this.gBoxComunicación.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Panel panelTitulo;
        private System.Windows.Forms.Panel panelContenido;
        private System.Windows.Forms.GroupBox gBoxAD620;
        private System.Windows.Forms.GroupBox gBoxHX711;
        private System.Windows.Forms.GroupBox gBoxComunicación;
        private System.Windows.Forms.Button btnStopHX;
        private System.Windows.Forms.Button btnStartHX;
        private System.Windows.Forms.ComboBox comBoxPuertos;
        private System.Windows.Forms.Button btnDesconectar;
        private System.Windows.Forms.Button btnConectar;
        private System.Windows.Forms.Button btnPuertos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblOffset;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.Label lblBits;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button btnStopAD620;
        private System.Windows.Forms.Button btnStartAD620;
        private System.Windows.Forms.Button btnP4;
        private System.Windows.Forms.Button btnP3;
        private System.Windows.Forms.Button btnP2;
        private System.Windows.Forms.Button btnP1;
        private System.Windows.Forms.Button btnP5;
        private System.Windows.Forms.Label label2;
        private System.IO.Ports.SerialPort mySerial;
        private System.Windows.Forms.Label lblAD620Factor;
        private System.Windows.Forms.Label lblAD620Offset;
        private System.Windows.Forms.Label lblAD620Peso;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblHXFactor;
        private System.Windows.Forms.Label lblHXOffset;
        private System.Windows.Forms.Label lblHXPeso;
        private System.Windows.Forms.Label lblHXBits;
    }
}

