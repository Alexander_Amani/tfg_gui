﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PruebaTFG_2
{
    public partial class FormMain : Form
    {
        public enum caracteres {
            INIT_CHAR = (int) '#', //caracter de inicio
            END_CHAR = (int) '@', //caracter de fin

            REPOSO_CHAR = (int)'r', //ir a estado de reposo
            HX_CHAR = (int)'h', //ir al estado de toma de datos del HX711
            HX_TARA = (int)'t', //realizamos la tara (igual quitamos esto)
            HX_SCALE = (int)'s', //realizamos el calibrado automatico del factor de escala (igual se quita esto)
            INA_CHAR = (int)'i', //ir al estado del amplificador

            OFFSET_CHAR = (int)'o', //ajuste manual del offset (slider)
            FACTOR_CHAR = (int)'f', //ajusto manual del factor de escala (slider)
            RESET_CHAR = (int)'z', //se pone a 0 el offset y a 1 el factor de escala
            GAIN_CHAR = (int)'g', //cambiamos la ganancia entre 128 o 64
            CONECT_CHAR = (int)'c', //mensaje para comprobar si estamos conectados al micro correctamente

            INA_DATA_CHAR=(int)'I',
            HX_DATA_CHAR=(int)'H'
        }
        //instancias de los forms hijos:
        public Form childForm;
        //delegado para el handler del serial:
        public delegate void d1(string dataIn); //necesario por el multithreading

        public FormMain()
        {
            InitializeComponent();
        }

        private void btnP1_Click(object sender, EventArgs e)
        {

            openChild(new FormP1(this));
        }

        private void btnP2_Click(object sender, EventArgs e)
        {
            openChild(new FormP2(this));
        }

        private void btnP3_Click(object sender, EventArgs e)
        {
            openChild(new FormP3(this));
        }

        private void btnP4_Click(object sender, EventArgs e)
        {
            openChild(new FormP4(this));
        }

        private void btnP5_Click(object sender, EventArgs e)
        {
            openChild(new FormP5(this));
        }

        private void openChild(Form child) {
            /*Con esta función se abre el hijo especificado
             * primero se comprueba que no esté ya abierto y que solo haya un hijo
             */
            bool isOpen = false; //nos dice si ya está abierto

            foreach (Form f in Application.OpenForms)
            {
                if (f.Name == child.Name)
                {
                    f.Focus();
                    isOpen = true;
                    break;
                }
            }

            if (isOpen == false) {
                if (Application.OpenForms.Count > 1)
                    childForm.Close(); //cerramos el hijo que habia
                childForm = child;
                childForm.Show();
            }
        }


        public void buscarPuertos() {
            /*
             *En esta función buscamos los puertos que hay disponibles en el ordenador
             * si hay algun puerto mostramos el primero de la lista
             * sino hay ninguno lanzamos mensaje de error
             */
            //limpiamos el comboBox:
            comBoxPuertos.Items.Clear();
            comBoxPuertos.Text = "";
            //obtenemos los puertos y los escribimos en el combox:
            string[] puertos = SerialPort.GetPortNames();
            comBoxPuertos.Items.AddRange(puertos);
            //seleccionamos el primero para que aparezca en la lista:
            if (comBoxPuertos.Items.Count > 0) {
                comBoxPuertos.SelectedIndex = 0;
            }
            else
            {  
                //si no hay nada conectado mostramos un mensaje de error:
                MessageBox.Show("No hay puertos disponibles", "ERROR PUERTOS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void mandarMensaje(string mensaje) {
            /*En esta función se gestiona el envio de mensajes por el puerto serie
             * antes de enviar es bueno comprobar que el puerto está abierto
             */
            if (mySerial.IsOpen) {
                mySerial.Write(mensaje);
            }
        }

        public void estadoAD620() {
            /*función para enviar el mensaje de inicio del AD620
             */
            // hay que construir el mensaje. convertimos enum en char y de ahi en string y concatenamos
            string mensaje = ((char)caracteres.INIT_CHAR).ToString();
            mensaje += ((char)caracteres.INA_CHAR).ToString();
            mensaje += ((char)caracteres.END_CHAR).ToString();

            //enviamos el mensaje:
            mandarMensaje(mensaje);
        }

        public void estadoReposo() {
            /* función para volver al estado de reposo
             */
            // hay que construir el mensaje. convertimos enum en char y de ahi en string y concatenamos
            string mensaje = ((char)caracteres.INIT_CHAR).ToString();
            mensaje += ((char)caracteres.REPOSO_CHAR).ToString();
            mensaje += ((char)caracteres.END_CHAR).ToString();

            //enviamos el mensaje:
            mandarMensaje(mensaje);
        }

        public void estadoHX()
        {
            /* función para ir al estado del HX711
             */
            // hay que construir el mensaje. convertimos enum en char y de ahi en string y concatenamos
            string mensaje = ((char)caracteres.INIT_CHAR).ToString();
            mensaje += ((char)caracteres.HX_CHAR).ToString();
            mensaje += ((char)caracteres.END_CHAR).ToString();

            //enviamos el mensaje:
            mandarMensaje(mensaje);
        }

        public void ajusteOffset(float value) {
            /*función para ajustar el offset del AD620 o del HX711
             */
            // hay que construir el mensaje. convertimos enum en char y de ahi en string y concatenamos
            string mensaje = ((char)caracteres.INIT_CHAR).ToString();
            mensaje += ((char)caracteres.OFFSET_CHAR).ToString();
            mensaje += value.ToString();
            mensaje += ((char)caracteres.END_CHAR).ToString();

            //enviamos el mensaje:
            mandarMensaje(mensaje);
        }
        public void ajusteOffset(int value)
        {
            /*función para ajustar el offset del AD620 o del HX711
             */
            // hay que construir el mensaje. convertimos enum en char y de ahi en string y concatenamos
            string mensaje = ((char)caracteres.INIT_CHAR).ToString();
            mensaje += ((char)caracteres.OFFSET_CHAR).ToString();
            mensaje += value.ToString();
            mensaje += ((char)caracteres.END_CHAR).ToString();

            //enviamos el mensaje:
            mandarMensaje(mensaje);
        }

        public void ajusteFactor(float value)
        {
            /*función para ajustar el factor de escala del AD620 o del HX711
             */
            // hay que construir el mensaje. convertimos enum en char y de ahi en string y concatenamos
            string mensaje = ((char)caracteres.INIT_CHAR).ToString();
            mensaje += ((char)caracteres.FACTOR_CHAR).ToString();
            mensaje += value.ToString();
            mensaje += ((char)caracteres.END_CHAR).ToString();

            //enviamos el mensaje:
            mandarMensaje(mensaje);
        }

        public void ajusteGainHX(int value) {
            /*Función para ajustar la ganancia del HX711
             */
            if (value == 64 || value == 128) {
                // hay que construir el mensaje. convertimos enum en char y de ahi en string y concatenamos
                string mensaje = ((char)caracteres.INIT_CHAR).ToString();
                mensaje += ((char)caracteres.GAIN_CHAR).ToString();
                mensaje += value.ToString();
                mensaje += ((char)caracteres.END_CHAR).ToString();

                //enviamos el mensaje:
                mandarMensaje(mensaje);
            }
        }

        public void resetParam() {
            /*Función para resetear los valores de offset y ganancia del HX711 o el AD620
             * hay que tener en cuenta que resetea aquel aparato que esté activo en ese momento
             */
             //hay que construir el mandato:
            string mensaje = ((char)caracteres.INIT_CHAR).ToString();
            mensaje += ((char)caracteres.RESET_CHAR).ToString();
            mensaje += ((char)caracteres.END_CHAR).ToString();

            //enviamos el mensaje:
            mandarMensaje(mensaje);

        }

        public void conectarSerial() {
            /*En esta función se gestiona la conexión al puerto serie
             */
            //tratamos de conectarnos al puerto serie:
            try
            {
                mySerial.PortName = comBoxPuertos.Text; //seleccionamos el puerto
                mySerial.BaudRate = 9600; //de momento siempre esta velocidad
                mySerial.Open(); //abrimos el puerto
            }
            catch (Exception ex)
            {
                //en caso de error lanzamos un mensaje:
                MessageBox.Show("Error de conexión", "ERROR CONEXION", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void desconectarSerial() {
            /*En esta función gestionamos la desconexión del puerto
             * puede ser que se añada primero un mandato de REPOSO para el micro
             * de esta forma al desconectarnos se manda al micro entrar en estado de reposo
             */


             //AÑADIR MENSAJE DE REPOSO


            try
            {
                mySerial.Close();
            }
            catch (Exception Ex)
            {
                //en caso de error lanzamos un mensaje:
                MessageBox.Show("Error de desconexión", "ERROR DESCONEXION", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void mySerial_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            /*
             * Este Handler se dispara cada vez que se recibe AL MENOS un byte por el puerto serie.
             * se puede cambiar, pero casi mejor así. Aquí leeremos UNA LINEA (hasta \n)
             * e invocaremos el delegate para que el HILO PRINCIPAL procese la información
             * tal y como está es como mejor me ha funcionado.
             */
             
            string dataIn = mySerial.ReadLine(); //sacamos una linea del buffer:
            d1 delegado = new d1(readProcess); //creamos el delegado con la función "readProcess"
            Invoke(delegado, dataIn); //Invocamos el delegado y le pasado dataIn como argumento

        }

        private void readProcess(string dataIn) {
            /*Esta función es la que usa el delegado. recibe una cadena de caracteres
             * desde el hilo que procesa los datos del serial.
             * aquí debemos determinar que hacer con los datos que nos han llegado
             * pueden llegarnos de momento 3 tipos: H-;-;-; o I-;-;-; o CONECTADO 
             * es decir, datos del HX711, datos del AMP o confirmación de conexión
             */
            //TO DO

            //prueba:
            //txtPrueba.Text = dataIn;

            //en función del primer caracter decidimos:
            char c = dataIn[0];
            string subData = dataIn.Substring(1);
            string[] datos; //vector de arrays (vector de vectores)
            switch (c) {
                case (char)caracteres.INA_DATA_CHAR:
                    //recibimos datos del AD620: valor;offset;factor\n
                    datos = subData.Split(';'); //separamos los datos por ;
                    if (datos.Length == 3) {  //MUY IMPORTANTE SINO EXPLOTA (VALOR ; OFFSET ; FACTOR)
                        //escribimos en las etiquetas:
                        lblAD620Peso.Text = datos[0];
                        lblAD620Offset.Text = datos[1];
                        lblAD620Factor.Text = datos[2];
                    }
                    break;
                case (char)caracteres.HX_DATA_CHAR:
                    datos = subData.Split(';'); //separamos los datos por ;
                    if (datos.Length == 4) {  //MUY IMPORTANTE SINO EXPLOTA (VALOR ; OFFSET ; FACTOR)
                        //escribimos en las etiquetas:
                        lblHXBits.Text = datos[0];
                        lblHXPeso.Text = datos[1];
                        lblHXOffset.Text = datos[2];
                        lblHXFactor.Text = datos[3];
                    }
                    break;
                default:
                    break;
            }
        }

        private void btnPuertos_Click(object sender, EventArgs e)
        {
            /*handler del boton para buscar puertos
             */
            buscarPuertos();
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            /*handler del boton para conectarse al serial
             */
            conectarSerial();
        }

        private void btnDesconectar_Click(object sender, EventArgs e)
        {
            /*handler del boton para desconectarse del puertos serie
             */
            desconectarSerial();
        }

        private void btnStartAD620_Click(object sender, EventArgs e)
        {
            /*Este handler se encarga del boton que inicia la transmisión de datos del AD620:
             */
            estadoAD620();
            
        }

        private void btnStopAD620_Click(object sender, EventArgs e)
        {
            /*handler para el botón que nos permite volver al estado de reposo en el micro
             */
            estadoReposo();
        }

        private void btnStartHX_Click(object sender, EventArgs e)
        {
            /*handler del evento para ir al estado del HX711
             */
            estadoHX();
        }

        private void btnStopHX_Click(object sender, EventArgs e)
        {
            /*handler para volver al estado de reposo en panel HX
             */
            estadoReposo();
        }
    }
}
