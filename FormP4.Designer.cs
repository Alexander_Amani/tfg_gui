﻿namespace PruebaTFG_2
{
    partial class FormP4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormP4));
            this.panelTitulo = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panelControles = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnPrevio = new System.Windows.Forms.Button();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.panelPaginas = new System.Windows.Forms.Panel();
            this.panelPag1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panelPag2 = new System.Windows.Forms.Panel();
            this.pBADC = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panelPag3 = new System.Windows.Forms.Panel();
            this.pBEsquema = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panelPag4 = new System.Windows.Forms.Panel();
            this.btnStopAD620 = new System.Windows.Forms.Button();
            this.btnStartAD620 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panelPag5 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panelPag6 = new System.Windows.Forms.Panel();
            this.lblOffsetVal = new System.Windows.Forms.Label();
            this.lblOffsetMax = new System.Windows.Forms.Label();
            this.lblOffsetMin = new System.Windows.Forms.Label();
            this.tBOffsetAD620 = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.panelPag7 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panelPag8 = new System.Windows.Forms.Panel();
            this.lblFactorVal = new System.Windows.Forms.Label();
            this.lblFactorMax = new System.Windows.Forms.Label();
            this.lblFactorMin = new System.Windows.Forms.Label();
            this.tBFactor = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panelPag9 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panelTitulo.SuspendLayout();
            this.panelControles.SuspendLayout();
            this.panelPaginas.SuspendLayout();
            this.panelPag1.SuspendLayout();
            this.panelPag2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBADC)).BeginInit();
            this.panelPag3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBEsquema)).BeginInit();
            this.panelPag4.SuspendLayout();
            this.panelPag5.SuspendLayout();
            this.panelPag6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tBOffsetAD620)).BeginInit();
            this.panelPag7.SuspendLayout();
            this.panelPag8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tBFactor)).BeginInit();
            this.panelPag9.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTitulo
            // 
            this.panelTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelTitulo.Controls.Add(this.lblTitulo);
            this.panelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitulo.Location = new System.Drawing.Point(0, 0);
            this.panelTitulo.Name = "panelTitulo";
            this.panelTitulo.Size = new System.Drawing.Size(822, 100);
            this.panelTitulo.TabIndex = 2;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTitulo.Location = new System.Drawing.Point(301, 37);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(277, 29);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "PUENTE CON AD620";
            // 
            // panelControles
            // 
            this.panelControles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelControles.Controls.Add(this.btnReset);
            this.panelControles.Controls.Add(this.btnPrevio);
            this.panelControles.Controls.Add(this.btnSiguiente);
            this.panelControles.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControles.Location = new System.Drawing.Point(0, 518);
            this.panelControles.Name = "panelControles";
            this.panelControles.Size = new System.Drawing.Size(822, 100);
            this.panelControles.TabIndex = 4;
            // 
            // btnReset
            // 
            this.btnReset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnReset.Location = new System.Drawing.Point(627, 26);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(137, 44);
            this.btnReset.TabIndex = 4;
            this.btnReset.Text = "RESET";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnPrevio
            // 
            this.btnPrevio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnPrevio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrevio.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPrevio.Location = new System.Drawing.Point(93, 26);
            this.btnPrevio.Name = "btnPrevio";
            this.btnPrevio.Size = new System.Drawing.Size(137, 44);
            this.btnPrevio.TabIndex = 3;
            this.btnPrevio.Text = "Previo";
            this.btnPrevio.UseVisualStyleBackColor = true;
            this.btnPrevio.Click += new System.EventHandler(this.btnPrevio_Click);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiguiente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSiguiente.Location = new System.Drawing.Point(358, 26);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(137, 44);
            this.btnSiguiente.TabIndex = 2;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // panelPaginas
            // 
            this.panelPaginas.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelPaginas.Controls.Add(this.panelPag1);
            this.panelPaginas.Controls.Add(this.panelPag2);
            this.panelPaginas.Controls.Add(this.panelPag3);
            this.panelPaginas.Controls.Add(this.panelPag4);
            this.panelPaginas.Controls.Add(this.panelPag5);
            this.panelPaginas.Controls.Add(this.panelPag6);
            this.panelPaginas.Controls.Add(this.panelPag7);
            this.panelPaginas.Controls.Add(this.panelPag8);
            this.panelPaginas.Controls.Add(this.panelPag9);
            this.panelPaginas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPaginas.Location = new System.Drawing.Point(0, 100);
            this.panelPaginas.Name = "panelPaginas";
            this.panelPaginas.Size = new System.Drawing.Size(822, 418);
            this.panelPaginas.TabIndex = 6;
            // 
            // panelPag1
            // 
            this.panelPag1.BackColor = System.Drawing.Color.White;
            this.panelPag1.Controls.Add(this.label1);
            this.panelPag1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag1.Location = new System.Drawing.Point(0, 0);
            this.panelPag1.Name = "panelPag1";
            this.panelPag1.Size = new System.Drawing.Size(822, 418);
            this.panelPag1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(289, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Conecte el puente al AD620 \r\n";
            // 
            // panelPag2
            // 
            this.panelPag2.BackColor = System.Drawing.Color.White;
            this.panelPag2.Controls.Add(this.pBADC);
            this.panelPag2.Controls.Add(this.label2);
            this.panelPag2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag2.Location = new System.Drawing.Point(0, 0);
            this.panelPag2.Name = "panelPag2";
            this.panelPag2.Size = new System.Drawing.Size(822, 418);
            this.panelPag2.TabIndex = 4;
            // 
            // pBADC
            // 
            this.pBADC.Image = ((System.Drawing.Image)(resources.GetObject("pBADC.Image")));
            this.pBADC.Location = new System.Drawing.Point(222, 115);
            this.pBADC.Name = "pBADC";
            this.pBADC.Size = new System.Drawing.Size(356, 214);
            this.pBADC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pBADC.TabIndex = 3;
            this.pBADC.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(678, 50);
            this.label2.TabIndex = 2;
            this.label2.Text = "Teniendo en cuenta las características del ADC calcule su resolución\r\n\r\n";
            // 
            // panelPag3
            // 
            this.panelPag3.BackColor = System.Drawing.Color.White;
            this.panelPag3.Controls.Add(this.pBEsquema);
            this.panelPag3.Controls.Add(this.label3);
            this.panelPag3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag3.Location = new System.Drawing.Point(0, 0);
            this.panelPag3.Name = "panelPag3";
            this.panelPag3.Size = new System.Drawing.Size(822, 418);
            this.panelPag3.TabIndex = 5;
            // 
            // pBEsquema
            // 
            this.pBEsquema.Image = ((System.Drawing.Image)(resources.GetObject("pBEsquema.Image")));
            this.pBEsquema.Location = new System.Drawing.Point(141, 115);
            this.pBEsquema.Name = "pBEsquema";
            this.pBEsquema.Size = new System.Drawing.Size(517, 270);
            this.pBEsquema.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pBEsquema.TabIndex = 3;
            this.pBEsquema.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(665, 50);
            this.label3.TabIndex = 2;
            this.label3.Text = "Seleccione un valor de resistencia adecuado para aprovechar\r\ntodo el rango dinámi" +
    "co del ADC sin exceder la tensión de referencia";
            // 
            // panelPag4
            // 
            this.panelPag4.BackColor = System.Drawing.Color.White;
            this.panelPag4.Controls.Add(this.btnStopAD620);
            this.panelPag4.Controls.Add(this.btnStartAD620);
            this.panelPag4.Controls.Add(this.label4);
            this.panelPag4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag4.Location = new System.Drawing.Point(0, 0);
            this.panelPag4.Name = "panelPag4";
            this.panelPag4.Size = new System.Drawing.Size(822, 418);
            this.panelPag4.TabIndex = 7;
            // 
            // btnStopAD620
            // 
            this.btnStopAD620.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnStopAD620.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStopAD620.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopAD620.Location = new System.Drawing.Point(17, 157);
            this.btnStopAD620.Name = "btnStopAD620";
            this.btnStopAD620.Size = new System.Drawing.Size(137, 44);
            this.btnStopAD620.TabIndex = 4;
            this.btnStopAD620.Text = "STOP";
            this.btnStopAD620.UseVisualStyleBackColor = true;
            this.btnStopAD620.Click += new System.EventHandler(this.btnStopAD620_Click);
            // 
            // btnStartAD620
            // 
            this.btnStartAD620.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnStartAD620.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartAD620.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartAD620.Location = new System.Drawing.Point(17, 98);
            this.btnStartAD620.Name = "btnStartAD620";
            this.btnStartAD620.Size = new System.Drawing.Size(137, 44);
            this.btnStartAD620.TabIndex = 3;
            this.btnStartAD620.Text = "START";
            this.btnStartAD620.UseVisualStyleBackColor = true;
            this.btnStartAD620.Click += new System.EventHandler(this.btnStartAD620_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(719, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "Conecte al micro la salida del amplificador e inicie la transmisión de datos";
            // 
            // panelPag5
            // 
            this.panelPag5.BackColor = System.Drawing.Color.White;
            this.panelPag5.Controls.Add(this.label7);
            this.panelPag5.Controls.Add(this.label6);
            this.panelPag5.Controls.Add(this.label5);
            this.panelPag5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag5.Location = new System.Drawing.Point(0, 0);
            this.panelPag5.Name = "panelPag5";
            this.panelPag5.Size = new System.Drawing.Size(822, 418);
            this.panelPag5.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(287, 25);
            this.label7.TabIndex = 4;
            this.label7.Text = "¿Está equilibrado el puente?\r\n";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(393, 25);
            this.label6.TabIndex = 3;
            this.label6.Text = "Observe la tensión de salida del puente";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(301, 25);
            this.label5.TabIndex = 2;
            this.label5.Text = "Quite todo de la viga de carga";
            // 
            // panelPag6
            // 
            this.panelPag6.BackColor = System.Drawing.Color.White;
            this.panelPag6.Controls.Add(this.lblOffsetVal);
            this.panelPag6.Controls.Add(this.lblOffsetMax);
            this.panelPag6.Controls.Add(this.lblOffsetMin);
            this.panelPag6.Controls.Add(this.tBOffsetAD620);
            this.panelPag6.Controls.Add(this.label10);
            this.panelPag6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag6.Location = new System.Drawing.Point(0, 0);
            this.panelPag6.Name = "panelPag6";
            this.panelPag6.Size = new System.Drawing.Size(822, 418);
            this.panelPag6.TabIndex = 9;
            // 
            // lblOffsetVal
            // 
            this.lblOffsetVal.AutoSize = true;
            this.lblOffsetVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOffsetVal.Location = new System.Drawing.Point(403, 140);
            this.lblOffsetVal.Name = "lblOffsetVal";
            this.lblOffsetVal.Size = new System.Drawing.Size(17, 17);
            this.lblOffsetVal.TabIndex = 19;
            this.lblOffsetVal.Text = "0";
            // 
            // lblOffsetMax
            // 
            this.lblOffsetMax.AutoSize = true;
            this.lblOffsetMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOffsetMax.Location = new System.Drawing.Point(745, 115);
            this.lblOffsetMax.Name = "lblOffsetMax";
            this.lblOffsetMax.Size = new System.Drawing.Size(61, 17);
            this.lblOffsetMax.TabIndex = 18;
            this.lblOffsetMax.Text = "label14";
            // 
            // lblOffsetMin
            // 
            this.lblOffsetMin.AutoSize = true;
            this.lblOffsetMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOffsetMin.Location = new System.Drawing.Point(19, 115);
            this.lblOffsetMin.Name = "lblOffsetMin";
            this.lblOffsetMin.Size = new System.Drawing.Size(61, 17);
            this.lblOffsetMin.TabIndex = 17;
            this.lblOffsetMin.Text = "label12";
            // 
            // tBOffsetAD620
            // 
            this.tBOffsetAD620.BackColor = System.Drawing.Color.White;
            this.tBOffsetAD620.Cursor = System.Windows.Forms.Cursors.Default;
            this.tBOffsetAD620.Location = new System.Drawing.Point(95, 95);
            this.tBOffsetAD620.Maximum = 1000;
            this.tBOffsetAD620.Minimum = -1000;
            this.tBOffsetAD620.Name = "tBOffsetAD620";
            this.tBOffsetAD620.Size = new System.Drawing.Size(632, 56);
            this.tBOffsetAD620.TabIndex = 16;
            this.tBOffsetAD620.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tBOffsetAD620.ValueChanged += new System.EventHandler(this.tBOffsetAD620_ValueChanged);
            this.tBOffsetAD620.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tBOffsetAD620_KeyUp);
            this.tBOffsetAD620.MouseCaptureChanged += new System.EventHandler(this.tBOffsetAD620_MouseCaptureChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(637, 25);
            this.label10.TabIndex = 2;
            this.label10.Text = "Mediante el deslizador, ajuste la tensión para equilibrar el puente";
            // 
            // panelPag7
            // 
            this.panelPag7.BackColor = System.Drawing.Color.White;
            this.panelPag7.Controls.Add(this.label8);
            this.panelPag7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag7.Location = new System.Drawing.Point(0, 0);
            this.panelPag7.Name = "panelPag7";
            this.panelPag7.Size = new System.Drawing.Size(822, 418);
            this.panelPag7.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(633, 25);
            this.label8.TabIndex = 2;
            this.label8.Text = "Coloque los pesos calibrados y obtenga una recta de calibración";
            // 
            // panelPag8
            // 
            this.panelPag8.BackColor = System.Drawing.Color.White;
            this.panelPag8.Controls.Add(this.lblFactorVal);
            this.panelPag8.Controls.Add(this.lblFactorMax);
            this.panelPag8.Controls.Add(this.lblFactorMin);
            this.panelPag8.Controls.Add(this.tBFactor);
            this.panelPag8.Controls.Add(this.label11);
            this.panelPag8.Controls.Add(this.label9);
            this.panelPag8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag8.Location = new System.Drawing.Point(0, 0);
            this.panelPag8.Name = "panelPag8";
            this.panelPag8.Size = new System.Drawing.Size(822, 418);
            this.panelPag8.TabIndex = 11;
            // 
            // lblFactorVal
            // 
            this.lblFactorVal.AutoSize = true;
            this.lblFactorVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFactorVal.Location = new System.Drawing.Point(402, 201);
            this.lblFactorVal.Name = "lblFactorVal";
            this.lblFactorVal.Size = new System.Drawing.Size(17, 17);
            this.lblFactorVal.TabIndex = 20;
            this.lblFactorVal.Text = "1";
            // 
            // lblFactorMax
            // 
            this.lblFactorMax.AutoSize = true;
            this.lblFactorMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFactorMax.Location = new System.Drawing.Point(755, 164);
            this.lblFactorMax.Name = "lblFactorMax";
            this.lblFactorMax.Size = new System.Drawing.Size(61, 17);
            this.lblFactorMax.TabIndex = 19;
            this.lblFactorMax.Text = "label14";
            // 
            // lblFactorMin
            // 
            this.lblFactorMin.AutoSize = true;
            this.lblFactorMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFactorMin.Location = new System.Drawing.Point(12, 164);
            this.lblFactorMin.Name = "lblFactorMin";
            this.lblFactorMin.Size = new System.Drawing.Size(61, 17);
            this.lblFactorMin.TabIndex = 18;
            this.lblFactorMin.Text = "label12";
            // 
            // tBFactor
            // 
            this.tBFactor.BackColor = System.Drawing.Color.White;
            this.tBFactor.Cursor = System.Windows.Forms.Cursors.Default;
            this.tBFactor.Location = new System.Drawing.Point(83, 144);
            this.tBFactor.Maximum = 5000;
            this.tBFactor.Minimum = 10;
            this.tBFactor.Name = "tBFactor";
            this.tBFactor.Size = new System.Drawing.Size(655, 56);
            this.tBFactor.TabIndex = 17;
            this.tBFactor.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tBFactor.Value = 10;
            this.tBFactor.ValueChanged += new System.EventHandler(this.tBFactor_ValueChanged);
            this.tBFactor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tBFactor_KeyUp);
            this.tBFactor.MouseCaptureChanged += new System.EventHandler(this.tBFactor_MouseCaptureChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(646, 25);
            this.label11.TabIndex = 3;
            this.label11.Text = "Con los pesos calibrados y el deslizador ajuste el factor de escala";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(525, 25);
            this.label9.TabIndex = 2;
            this.label9.Text = "Se desea obtener una salida directamente en gramos";
            // 
            // panelPag9
            // 
            this.panelPag9.BackColor = System.Drawing.Color.White;
            this.panelPag9.Controls.Add(this.label13);
            this.panelPag9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag9.Location = new System.Drawing.Point(0, 0);
            this.panelPag9.Name = "panelPag9";
            this.panelPag9.Size = new System.Drawing.Size(822, 418);
            this.panelPag9.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(12, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(508, 25);
            this.label13.TabIndex = 2;
            this.label13.Text = "Cambie el valor de la resistencia y repita el proceso";
            // 
            // FormP4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 618);
            this.Controls.Add(this.panelPaginas);
            this.Controls.Add(this.panelControles);
            this.Controls.Add(this.panelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormP4";
            this.Text = "FormP4";
            this.panelTitulo.ResumeLayout(false);
            this.panelTitulo.PerformLayout();
            this.panelControles.ResumeLayout(false);
            this.panelPaginas.ResumeLayout(false);
            this.panelPag1.ResumeLayout(false);
            this.panelPag1.PerformLayout();
            this.panelPag2.ResumeLayout(false);
            this.panelPag2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBADC)).EndInit();
            this.panelPag3.ResumeLayout(false);
            this.panelPag3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBEsquema)).EndInit();
            this.panelPag4.ResumeLayout(false);
            this.panelPag4.PerformLayout();
            this.panelPag5.ResumeLayout(false);
            this.panelPag5.PerformLayout();
            this.panelPag6.ResumeLayout(false);
            this.panelPag6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tBOffsetAD620)).EndInit();
            this.panelPag7.ResumeLayout(false);
            this.panelPag7.PerformLayout();
            this.panelPag8.ResumeLayout(false);
            this.panelPag8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tBFactor)).EndInit();
            this.panelPag9.ResumeLayout(false);
            this.panelPag9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTitulo;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel panelControles;
        private System.Windows.Forms.Button btnPrevio;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Panel panelPaginas;
        private System.Windows.Forms.Panel panelPag6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panelPag1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelPag2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelPag3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelPag4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelPag5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panelPag8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelPag7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TrackBar tBOffsetAD620;
        private System.Windows.Forms.TrackBar tBFactor;
        private System.Windows.Forms.Panel panelPag9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnStopAD620;
        private System.Windows.Forms.Button btnStartAD620;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lblOffsetVal;
        private System.Windows.Forms.Label lblOffsetMax;
        private System.Windows.Forms.Label lblOffsetMin;
        private System.Windows.Forms.Label lblFactorVal;
        private System.Windows.Forms.Label lblFactorMax;
        private System.Windows.Forms.Label lblFactorMin;
        private System.Windows.Forms.PictureBox pBADC;
        private System.Windows.Forms.PictureBox pBEsquema;
    }
}