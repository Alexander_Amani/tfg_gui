﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaTFG_2
{
    public partial class FormP5 : Form
    {
        //lista con todos los paneles(páginas) del form:
        Paginas paginas;

        //Form padre:
        private FormMain FormFather;

        public FormP5(FormMain father)
        {
            InitializeComponent();
            ListaPanelesInit();
            //valores de los labels:
            /*
            lblOffsetMin.Text = ((float)tBOffset.Minimum / 10).ToString();
            lblOffsetMax.Text = ((float)tBOffset.Maximum / 10).ToString();
            lblFactorMin.Text = ((float)tBFactor.Minimum / 10).ToString();
            lblFactorMax.Text= ((float)tBFactor.Maximum / 10).ToString();
            */
            //probamos sin dividir:
            lblOffsetMin.Text = (tBOffset.Minimum).ToString();
            lblOffsetMax.Text = (tBOffset.Maximum).ToString();
            lblFactorMin.Text = (tBFactor.Minimum).ToString();
            lblFactorMax.Text= (tBFactor.Maximum).ToString();
            //
            FormFather = father;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            paginas.siguiente();
        }

        private void btnPrevio_Click(object sender, EventArgs e)
        {
            paginas.anterior();
        }

        private void ListaPanelesInit()
        {
            //aqui inicializamos la lista de paneles (paginas):
            paginas = new Paginas();
            //agregamos todas las páginas que haya en panelPaginas.controles:
            foreach (Panel p in this.panelPaginas.Controls)
            {
                paginas.agregar(p);
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            /*Handler del botón que manda al estado HX
             */
            FormFather.estadoHX();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            /*Volvemos al estado de reposo
             */
            FormFather.estadoReposo();
        }

        private void tBOffset_MouseCaptureChanged(object sender, EventArgs e)
        {
            /* captura el evento de SOLTAR EL BOTÓN DEL RATÓN
             * no usar scroll porque manda muchos mensajes!
             */
            int value = tBOffset.Value;
            FormFather.ajusteOffset(value);
        }

        private void tBOffset_KeyUp(object sender, KeyEventArgs e)
        {
            /* captura el evento de SOLTAR LA TECLA DE LAS FLECHAS
             * no usar scroll porque manda muchos mensajes!
             */
            int value = tBOffset.Value;
            FormFather.ajusteOffset(value);
        }

        private void tBFactor_MouseCaptureChanged(object sender, EventArgs e)
        {
            /* captura el evento de SOLTAR EL BOTÓN DEL RATÓN
             * no usar scroll porque manda muchos mensajes!
             */
            //el slider solo permite valores enteros, dividimos por factor:
            //float value = (float)tBFactor.Value/10;
            //sin dividir:
            float value = tBFactor.Value;
            FormFather.ajusteFactor(value);
        }

        private void tBFactor_KeyUp(object sender, KeyEventArgs e)
        {
            /* captura el evento de SOLTAR LA TECLA DE LAS FLECHAS
             * no usar scroll porque manda muchos mensajes!
             */
            //el slider solo permite valores enteros, dividimos por factor:
            //float value = (float)tBFactor.Value / 10;
            //sin dividir:
            float value = tBFactor.Value;
            FormFather.ajusteFactor(value);
        }

        private void rBGain128_CheckedChanged(object sender, EventArgs e)
        {
            /*evento de cambiar el valor del radiobutton de 128
             * realmente solo hace falta uno ya que cuando está seleccionado uno
             * el resto no lo están por lo que si cambia 1, cambian todos
             */
            if (rBGain128.Checked)
            {
                FormFather.ajusteGainHX(128);
            }
            else if (rBGain64.Checked) {
                FormFather.ajusteGainHX(64);
            }
        }

        private void tBOffset_ValueChanged(object sender, EventArgs e)
        {
            //cambiamos el valor del label valor:
            //lblOffsetVal.Text = ((float)tBOffset.Value / 10).ToString();
            //sin dividir:
            lblOffsetVal.Text = (tBOffset.Value).ToString();
        }

        private void tBFactor_ValueChanged(object sender, EventArgs e)
        {
            //lblFactorVal.Text = ((float)tBFactor.Value / 10).ToString();
            lblFactorVal.Text = (tBFactor.Value).ToString();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            FormFather.resetParam();
        }
    }
}
