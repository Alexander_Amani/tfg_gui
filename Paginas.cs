﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaTFG_2
{
    class Paginas
    {
        /*En esta clase se implementa la lista de páginas (paneles)
         */
        private List<Panel> listaPaneles; //lista de paneles
        private int listaIndex; //índice de página

        public Paginas() {
            //constructor:
            listaPaneles = new List<Panel>();
            listaIndex = 0; //iniciamos en página 0
        }

        public void agregar(Panel panel) {
            //insertamos una página (panel en la lista):
            listaPaneles.Add(panel);
            if (listaPaneles.Count == 1)
                listaPaneles[listaIndex].BringToFront(); //si es el primer panel que metemos lo mostramos
        }

        public void siguiente() {
            //pasamos a la siguiente página:
            if (listaIndex < listaPaneles.Count - 1)
            { //mientras no esté en la última página
                listaPaneles[++listaIndex].BringToFront();
            }
        }

        public void anterior()
        {
            //pasamos a la anterior página:
            if (listaIndex > 0)
            { //mientras no esté en la primera página:
                listaPaneles[--listaIndex].BringToFront();
            }
        }
    }
}
