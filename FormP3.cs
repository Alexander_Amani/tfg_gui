﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaTFG_2
{
    public partial class FormP3 : Form
    {
        //lista con todos los paneles(páginas) del form:
        Paginas paginas;
        //Form padre:
        private FormMain FormFather;

        public FormP3(FormMain father)
        {
            InitializeComponent();
            ListaPanelesInit();

            //
            FormFather = father;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            paginas.siguiente();
        }

        private void btnPrevio_Click(object sender, EventArgs e)
        {
            paginas.anterior();
        }

        private void ListaPanelesInit()
        {
            //aqui inicializamos la lista de paneles (paginas):
            paginas = new Paginas();
            //agregamos todas las páginas que haya en panelPaginas.controles:
            foreach (Panel p in this.panelPaginas.Controls)
            {
                paginas.agregar(p);
            }
        }

        private void btnPuertos_Click(object sender, EventArgs e)
        {
            /*
             * en este handler gestionamos el evento de selección de puertos:
             */
            FormFather.buscarPuertos();
            
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            /*este es el handler del boton de conexión al puerto serie
             */
            FormFather.conectarSerial();
        }

        private void btnDesconectar_Click(object sender, EventArgs e)
        {
            /*este es el handler del boton de desconexión al puerto serie
             */
            FormFather.desconectarSerial();
        }
    }
}
