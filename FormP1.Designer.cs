﻿namespace PruebaTFG_2
{
    partial class FormP1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormP1));
            this.panelTitulo = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panelPag1 = new System.Windows.Forms.Panel();
            this.pBoffset = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelControles = new System.Windows.Forms.Panel();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.btnPrevio = new System.Windows.Forms.Button();
            this.panelPag2 = new System.Windows.Forms.Panel();
            this.pBEcCMRR = new System.Windows.Forms.PictureBox();
            this.pBCMRR = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panelPaginas = new System.Windows.Forms.Panel();
            this.panelTitulo.SuspendLayout();
            this.panelPag1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoffset)).BeginInit();
            this.panelControles.SuspendLayout();
            this.panelPag2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBEcCMRR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBCMRR)).BeginInit();
            this.panelPaginas.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTitulo
            // 
            this.panelTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelTitulo.Controls.Add(this.lblTitulo);
            this.panelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitulo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelTitulo.Location = new System.Drawing.Point(0, 0);
            this.panelTitulo.Name = "panelTitulo";
            this.panelTitulo.Size = new System.Drawing.Size(992, 93);
            this.panelTitulo.TabIndex = 0;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(323, 29);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(355, 29);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "CARACTERIZACIÓN AD620";
            // 
            // panelPag1
            // 
            this.panelPag1.BackColor = System.Drawing.Color.White;
            this.panelPag1.Controls.Add(this.pBoffset);
            this.panelPag1.Controls.Add(this.label5);
            this.panelPag1.Controls.Add(this.label4);
            this.panelPag1.Controls.Add(this.label3);
            this.panelPag1.Controls.Add(this.label2);
            this.panelPag1.Controls.Add(this.label1);
            this.panelPag1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag1.Location = new System.Drawing.Point(0, 0);
            this.panelPag1.Name = "panelPag1";
            this.panelPag1.Size = new System.Drawing.Size(992, 528);
            this.panelPag1.TabIndex = 1;
            // 
            // pBoffset
            // 
            this.pBoffset.Image = ((System.Drawing.Image)(resources.GetObject("pBoffset.Image")));
            this.pBoffset.Location = new System.Drawing.Point(719, 200);
            this.pBoffset.Name = "pBoffset";
            this.pBoffset.Size = new System.Drawing.Size(261, 286);
            this.pBoffset.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pBoffset.TabIndex = 5;
            this.pBoffset.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(175, 25);
            this.label5.TabIndex = 4;
            this.label5.Text = "INPUT OFFSET:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 461);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(529, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Repita el proceso cambiando el valor de la resistencia\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 354);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(462, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "¿Cuál es el valor en la hoja de características?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(405, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "¿Cuál es el valor de la tensión de offset?";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(789, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cortocircuite las entradas del amplificador operaciona y mida la tensión de salid" +
    "a";
            // 
            // panelControles
            // 
            this.panelControles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(78)))), ((int)(((byte)(123)))));
            this.panelControles.Controls.Add(this.btnSiguiente);
            this.panelControles.Controls.Add(this.btnPrevio);
            this.panelControles.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControles.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelControles.Location = new System.Drawing.Point(0, 621);
            this.panelControles.Name = "panelControles";
            this.panelControles.Size = new System.Drawing.Size(992, 80);
            this.panelControles.TabIndex = 2;
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiguiente.Location = new System.Drawing.Point(535, 22);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(137, 44);
            this.btnSiguiente.TabIndex = 1;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // btnPrevio
            // 
            this.btnPrevio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(127)))), ((int)(((byte)(198)))));
            this.btnPrevio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrevio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevio.Location = new System.Drawing.Point(306, 22);
            this.btnPrevio.Name = "btnPrevio";
            this.btnPrevio.Size = new System.Drawing.Size(137, 44);
            this.btnPrevio.TabIndex = 0;
            this.btnPrevio.Text = "Previo";
            this.btnPrevio.UseVisualStyleBackColor = true;
            this.btnPrevio.Click += new System.EventHandler(this.btnPrevio_Click);
            // 
            // panelPag2
            // 
            this.panelPag2.BackColor = System.Drawing.Color.White;
            this.panelPag2.Controls.Add(this.pBEcCMRR);
            this.panelPag2.Controls.Add(this.pBCMRR);
            this.panelPag2.Controls.Add(this.label9);
            this.panelPag2.Controls.Add(this.label8);
            this.panelPag2.Controls.Add(this.label7);
            this.panelPag2.Controls.Add(this.label6);
            this.panelPag2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPag2.Location = new System.Drawing.Point(0, 0);
            this.panelPag2.Name = "panelPag2";
            this.panelPag2.Size = new System.Drawing.Size(992, 528);
            this.panelPag2.TabIndex = 2;
            // 
            // pBEcCMRR
            // 
            this.pBEcCMRR.Image = ((System.Drawing.Image)(resources.GetObject("pBEcCMRR.Image")));
            this.pBEcCMRR.Location = new System.Drawing.Point(37, 335);
            this.pBEcCMRR.Name = "pBEcCMRR";
            this.pBEcCMRR.Size = new System.Drawing.Size(382, 151);
            this.pBEcCMRR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pBEcCMRR.TabIndex = 10;
            this.pBEcCMRR.TabStop = false;
            // 
            // pBCMRR
            // 
            this.pBCMRR.Image = ((System.Drawing.Image)(resources.GetObject("pBCMRR.Image")));
            this.pBCMRR.Location = new System.Drawing.Point(660, 107);
            this.pBCMRR.Name = "pBCMRR";
            this.pBCMRR.Size = new System.Drawing.Size(308, 306);
            this.pBCMRR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pBCMRR.TabIndex = 9;
            this.pBCMRR.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(32, 255);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(462, 25);
            this.label9.TabIndex = 8;
            this.label9.Text = "¿Cuál es el valor de la hoja de características?";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(32, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(296, 25);
            this.label8.TabIndex = 7;
            this.label8.Text = "¿Cuál es el CMRR y el CMR?";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(32, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(551, 25);
            this.label7.TabIndex = 6;
            this.label7.Text = "Aplique la misma tensión a las entradas del amplificador";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(32, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(400, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "RECHAZO AL MODO COMÚN (CMRR):";
            // 
            // panelPaginas
            // 
            this.panelPaginas.BackColor = System.Drawing.SystemColors.Control;
            this.panelPaginas.Controls.Add(this.panelPag1);
            this.panelPaginas.Controls.Add(this.panelPag2);
            this.panelPaginas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPaginas.Location = new System.Drawing.Point(0, 93);
            this.panelPaginas.Name = "panelPaginas";
            this.panelPaginas.Size = new System.Drawing.Size(992, 528);
            this.panelPaginas.TabIndex = 3;
            // 
            // FormP1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 701);
            this.Controls.Add(this.panelPaginas);
            this.Controls.Add(this.panelControles);
            this.Controls.Add(this.panelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormP1";
            this.Text = "FormP1";
            this.Load += new System.EventHandler(this.FormP1_Load);
            this.panelTitulo.ResumeLayout(false);
            this.panelTitulo.PerformLayout();
            this.panelPag1.ResumeLayout(false);
            this.panelPag1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoffset)).EndInit();
            this.panelControles.ResumeLayout(false);
            this.panelPag2.ResumeLayout(false);
            this.panelPag2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBEcCMRR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBCMRR)).EndInit();
            this.panelPaginas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTitulo;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel panelPag1;
        private System.Windows.Forms.Panel panelControles;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Button btnPrevio;
        private System.Windows.Forms.Panel panelPag2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panelPaginas;
        private System.Windows.Forms.PictureBox pBoffset;
        private System.Windows.Forms.PictureBox pBEcCMRR;
        private System.Windows.Forms.PictureBox pBCMRR;
    }
}